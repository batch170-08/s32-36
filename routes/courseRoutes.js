const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")
const userController = require("../controllers/userController.js")

/*ADDING COURSE*/
router.post("/add-course", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})
/*RETRIEVE ALL ACTIVE COURSES*/
router.get("/active", (req,res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})

/*GETTING ALL COURSES*/
router.get("/", (req, res) =>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

/*GETTING COURSE USING ID*/
router.get("/:courseId", (req,res) => {
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


/*UPDATING A COURSE*/

router.put("/:courseId", auth.verify, (req, res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController)
	)
})


// Activity
/*ARCHIVING A COURSE*/
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController))
});


/*router.put("/edit", auth.verify, (req, res) =>{
	let updatedCourse = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	courseController.editCourse(updatedCourse).then( result => res.send(result))
})
*/

module.exports = router



