const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") //used to encrypt user passwords



/*1. check for the email in the database
2. send the result as a response (with error handling)*/
/*it is conventional for the devs to use Boolean in sending return responses esp with the backend application*/

module.exports.checkEmail = (requestBody) => {
	return User.find({email: requestBody.email}).then((result,error) =>{
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				//return result
				return true
			} else {
				//return res.send("email does not exist")
				return false
			}
		}
	})
}


// SIGN UP

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobile: reqBody.mobile,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 the number of rounds/times it runs the algorithm to the reqBody.password
			//max 72 implementations
		password: bcrypt.hashSync(reqBody.password, 10),
		
	})
	return newUser.save().then((saved, error) => {
		if (error) {
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

/*USER LOGIN
1. find if the email is existing in the database
2. check if the password is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result === null) {
			return false
		} else {
			// comparteSync function - used to compare a non-encrypted password to an encrypted password and returns a Boolean response, depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth-imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our create AccessToken
			} else {
				return false
			}
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		if (result === null) {
			console.log(error)
			return false
		} else {
			result.password = ""
			return result
		}
	})
}


module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {user.enrollments.push({courseId: data.courseId})
		return user.save().then((user, err) => {
			if (err) {
				console.log(err)
				return false
			} else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {course.enrollees.push({userId: data.userId})
		return course.save().then((result, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})
	
	if (isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}
}