const Course = require("../models/course.js")
const User = require("../models/user.js")

/*ADDING COURSE*/

module.exports.addCourse = (reqBody, data) => { 
	return User.findById(data.id).then(result => {
		if (result.isAdmin === false) {
			return "You are not an admin"
		} else {
			let newCourse = new Course ({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newCourse.save().then((res, err) =>{
				if (err) {
					console.log(err)
					return false
				} else {
					return "Course created successfully"
				}
			})
		}	
	})
}


/*GETTING ALL COURSES*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result=> {
		return result
	})
};


/*GETTING COURSE USING ID*/

module.exports.getCourse = (reqParams) => {
	console.log(reqParams.courseId)
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

/*RETRIEVE ALL ACTIVE COURSES*/

module.exports.getActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

/*UPDATING A COURSE*/

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

// Activity
/*ARCHIVING A COURSE*/

module.exports.archiveCourse = (reqParams, reqBody, userData) => {
	return User.findById(userData.id).then(result => {
		if (result.isAdmin === false) {
			return "You don't have access to make this changes"
		} else {
			let updatedCourse = {
				isActive: reqBody.isActive
			}
			return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((res, err) => {
				if (err) {
					console.log(error)
					return false
				} else {
					return true
				}
			})
		}
	})
}


/*
module.exports.editCourse = async (updatedCourse) => {
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {course.enrollees.push({userId: data.userId})
		return course.save().then((result, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	if (isCourseUpdated) {
		return true
	} else {
		return false
	}
}
*/